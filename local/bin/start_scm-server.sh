#!/bin/sh

set -e

#exec su-exec ${RUN_USER_NAME:-forumi0721} java -Dscm.home=/repo -Djava.awt.headless=true -Dlogback.configurationFile=logging.xml -classpath :/app/scm-server/conf:/app/scm-server/lib/scm-server-1.49.jar:/app/scm-server/lib/commons-daemon-1.0.15.jar:/app/scm-server/lib/jetty-server-7.6.19.v20160209.jar:/app/scm-server/lib/javax.servlet-2.5.0.v201103041518.jar:/app/scm-server/lib/jetty-continuation-7.6.19.v20160209.jar:/app/scm-server/lib/jetty-http-7.6.19.v20160209.jar:/app/scm-server/lib/jetty-io-7.6.19.v20160209.jar:/app/scm-server/lib/jetty-webapp-7.6.19.v20160209.jar:/app/scm-server/lib/jetty-xml-7.6.19.v20160209.jar:/app/scm-server/lib/jetty-servlet-7.6.19.v20160209.jar:/app/scm-server/lib/jetty-security-7.6.19.v20160209.jar:/app/scm-server/lib/jetty-jmx-7.6.19.v20160209.jar:/app/scm-server/lib/jetty-util-7.6.19.v20160209.jar:/app/scm-server/lib/jetty-ajp-7.6.19.v20160209.jar -Dapp.name=scm-server -Dapp.pid=1 -Dapp.repo=/app/scm-server/lib -Dbasedir=/app/scm-server sonia.scm.server.ScmServerDaemon
export SCM_HOME=${SCM_HOME:-/repo}
exec su-exec ${RUN_USER_NAME:-forumi0721} /app/scm-server/bin/scm-server

